use std::env;
use std::io::{self, Write};

const BUF_SIZE: usize = 32 * 1024;

fn text() -> String {
    format!("{}\n", env::args().nth(1).unwrap_or_else(|| "y".to_string()))
}

fn main() {
    let ys: Vec<_> = text().bytes().cycle().take(BUF_SIZE).collect();

    let stdout = io::stdout();
    let mut stdout = stdout.lock();

    loop {
        stdout.write_all(&ys).unwrap();
    }
}
